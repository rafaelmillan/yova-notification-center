Delivery.delete_all
Notification.delete_all
Client.delete_all
Admin.delete_all

admin = Admin.create!(email: "admin@example.com", password: "12345678", password_confirmation: "12345678")

client_1 = Client.create!(email: "client-1@example.com", password: "12345678", password_confirmation: "12345678")
client_2 = Client.create!(email: "client-2@example.com", password: "12345678", password_confirmation: "12345678")
client_3 = Client.create!(email: "client-3@example.com", password: "12345678", password_confirmation: "12345678")

notification_1 = admin.notifications.create!(title: "We’re excited to bring you onboard!", body: "Welcome to Yova Impact Investing – and well done for taking this opportunity to think about your finances! I’m Nicole, from Yova’s Customer Success Team. I’m here to answer all your questions and help with anything you need as we set up your account and get your impact investment started.")
notification_2 = admin.notifications.create!(title: "Something to share with the women in your life", body: "Here's an uncomfortable truth: women tend to have less money at retirement than men do. Think that’s unfair? So do we! Together with our community, we want to empower women of all ages and walks of life, by showing them that they can accumulate just as much wealth through investing as men do.")
notification_3 = admin.notifications.create!(title: "A special offer to kick-start your investment", body: "We get it. Investing is one of those tasks that’s easy to put off. We all know that money in a savings account loses value over time. At today's rates, CHF 10,000 in your bank account earns only CHF 1 in interest per year – then there's monthly fees and inflation to consider.")

Delivery.create!(notification: notification_1, client: client_1)
Delivery.create!(notification: notification_1, client: client_2)
Delivery.create!(notification: notification_1, client: client_3, seen: true)
Delivery.create!(notification: notification_2, client: client_3, seen:true)
Delivery.create!(notification: notification_3, client: client_1)
Delivery.create!(notification: notification_3, client: client_2)
Delivery.create!(notification: notification_3, client: client_3)
