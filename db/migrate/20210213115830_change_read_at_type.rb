class ChangeReadAtType < ActiveRecord::Migration[5.2]
  def change
    rename_column :notifications, :read_at, :seen
    change_column_default :notifications, :seen, from: nil, to: false
  end
end
