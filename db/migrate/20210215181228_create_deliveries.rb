class CreateDeliveries < ActiveRecord::Migration[5.2]
  def change
    create_table :deliveries do |t|
      t.belongs_to :client, foreign_key: true
      t.belongs_to :notification, foreign_key: true
      t.boolean :seen, default: false

      t.timestamps
    end

    remove_column :notifications, :seen, :boolean, default: false
    remove_reference :notifications, :client
  end
end
