class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.string :title, null: false
      t.text :body, null: false
      t.belongs_to :admin
      t.belongs_to :client
      t.datetime :deleted_at
      t.boolean :read_at

      t.timestamps
    end
  end
end
