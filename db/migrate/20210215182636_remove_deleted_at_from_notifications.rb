class RemoveDeletedAtFromNotifications < ActiveRecord::Migration[5.2]
  def change
    remove_column :notifications, :deleted_at, :datetime
  end
end
