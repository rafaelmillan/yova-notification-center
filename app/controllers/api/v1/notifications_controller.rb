class Api::V1::NotificationsController < ApplicationController
  before_action :authenticate_client!

  def index
    @deliveries = current_client.deliveries.includes(:notification)
  end

  def show
    @delivery = current_client.deliveries.find_by(notification_id: params[:id])

    if @delivery
      @delivery.update!(seen: true)
    else
      head :not_found
    end
  end
end
