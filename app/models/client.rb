class Client < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  self.skip_session_storage = [:http_auth, :params_auth]

  has_many :deliveries, dependent: :destroy
  has_many :notifications, through: :deliveries
end
