class Notification < ApplicationRecord
  validates :title, presence: true
  validates :body, presence: true

  belongs_to :admin

  has_many :deliveries, dependent: :destroy
  has_many :clients, through: :deliveries
end
