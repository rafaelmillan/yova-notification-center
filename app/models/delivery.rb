class Delivery < ApplicationRecord
  belongs_to :client
  belongs_to :notification

  delegate :title, to: :notification
end
