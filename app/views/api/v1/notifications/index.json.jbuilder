json.array! @deliveries do |delivery|
  json.id delivery.notification.id
  json.title delivery.notification.title
  json.seen delivery.seen
end
