# Yova Notification Center

This web app simulates a notification system where admins can create messages for one or multiple clients through a back office. The clients can then access the notifications through a REST API, while the admins can check if the clients have seen the notification.

## Implementation details

The data model is flexible enough to allow one admin to create a single notification for one or multiple clients. This way they can create personal messages as well as bulk notifications.

The notification "seen" status is persisted in a `deliveries` table which provides a HABTM relationship between notifications and clients.

When a client fetches the notifications index, it returns the titles but not the bodies. Once the clients retrieve a specific notification they can see the body, which marks the notification as "seen".

### Gems

The admins and clients both use [Devise](https://github.com/heartcombo/devise) for authentication. The admin framework used in the back office is [Administrate](https://github.com/thoughtbot/administrate).

## Demo
You can play around with the app in the [live demo](http://yova-notification-center.herokuapp.com). Credentials can be found in the [seed file](https://github.com/rafaelmillan/yova-notification-center/blob/main/db/seeds.rb).

### Admin (back office)
Visit [/tools](http://yova-notification-center.herokuapp.com/tools) to play with the back office.

![Back office](./public/backoffice.png)

### Client (API)
Authenticate using [basic HTTP auth](https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication#basic_authentication_scheme).

**Retrieve all notifications**

```
GET /api/v1/notifications
```

Response:

```
[
    {
        "id": 17,
        "title": "We’re excited to bring you onboard!",
        "seen": true
    },
    {
        "id": 19,
        "title": "A special offer to kick-start your investment",
        "seen": false
    }
]
```

**Retrieve a notification and mark it as seen**

```
GET /api/v1/notifications/:id
```

Response:

```
{
    "id": 1,
    "title": "We’re excited to bring you onboard!",
    "body": "Welcome to Yova Impact Investing – and well done for taking this opportunity to think about your finances! I’m Nicole, from Yova’s Customer Success Team. I’m here to answer all your questions and help with anything you need as we set up your account and get your impact investment started.",
    "seen": true
}
```

## Local development
To test locally clone this repo and run:

```
bundle
rails db:setup
```

To run tests:

```
bundle exec rspec
```
