require "rails_helper"

RSpec.describe "Notifications" do
  let(:headers) do
    {
      "HTTP_AUTHORIZATION" => ActionController::HttpAuthentication::Basic.encode_credentials(client.email, "secret")
    }
  end
  let(:client) { create(:client) }
  let!(:delivery) { create(:delivery, client: client) }
  let!(:not_my_delivery) { create(:delivery) }

  describe "#index" do
    it "returns the client's notifications" do
      get "/api/v1/notifications", headers: headers

      expect(JSON.parse(response.body).first).to eq(
        {
          "id" => delivery.notification.id,
          "title" => delivery.notification.title,
          "seen" => false
        }
      )
    end
  end

  describe "#show" do
    it "returns the client's notification and marks it as seen" do
      get "/api/v1/notifications/#{delivery.id}", headers: headers

      expect(JSON.parse(response.body)).to eq(
        {
          "id" => delivery.notification.id,
          "title" => delivery.notification.title,
          "body" => delivery.notification.body,
          "seen" => true
        }
      )
    end

    context "when it can't find the notification" do
      it "returns a 404" do
        get "/api/v1/notifications/#{not_my_delivery.id}", headers: headers

        expect(JSON.parse(response.code)).to eq(404)
      end
    end
  end
end
