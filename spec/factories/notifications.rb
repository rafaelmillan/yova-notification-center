FactoryBot.define do
  factory :notification do
    title { "We’re excited to bring you onboard!" }
    body  { "Well done for taking this opportunity to think about your finances!" }

    admin
  end
end
