FactoryBot.define do
  factory :admin do
    sequence(:email){|n| "george#{n}@vandelay-industries.com" }
    password  { "secret" }
    password_confirmation { "secret" }
  end
end
