FactoryBot.define do
  factory :client do
    sequence(:email){|n| "cosmo#{n}@kramer.biz" }
    password  { "secret" }
    password_confirmation { "secret" }
  end
end
