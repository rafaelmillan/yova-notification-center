Rails.application.routes.draw do
  namespace :tools do
    resources :notifications
    resources :clients
    resources :admins
    resources :deliveries, only: [:index, :show]

    root to: "notifications#index"
  end

  devise_for :clients, defaults: { format: :json }
  devise_for :admins

  root to: "pages#home"

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :notifications, only: [:index, :show]
    end
  end
end
